MNIST example
*************

Introduction
============

The MNIST database (Modified National Institute of Standards and Technology database) is a dataset of handwritten digits which can be used for training various image processing systems. 

In this chapter we will use MNIST data to understand the basic flow of the tensorflow coding. More specifically, we will use the Gradient-optimizer in the tensorflow graph to predict the hand-written digits. 



Read and Plot numbers from "mnist" dataset
==========================================


.. code-block:: python
    :linenos:
    :caption: Plots the test data from mnist datasets

    # mnist_ex.py

    # MNIST dataset is a collection of hand-written digits-images. 
    # Training set = 55000
    # Test set = 10000
    # Validation set = 5000

    import numpy as np
    import tensorflow as tf

    from tensorflow.examples.tutorials.mnist import input_data

    from helper_functions import plot_mnist_image


    # data will be donwloaded to './data/MNIST' 
    # one_hot convert each number to vector with single 1, 
    # e.g. 0 = [1, 0, 0, 0, 0, 0, 0, 0]  (assuming total 8 labels)
    # and 4 = [0, 0, 0, 0, 1, 0, 0, 0]

    # # without oneshot labels
    # data = input_data.read_data_sets("data/MNIST/")
    # print("label for first 5 elements:\n {}".format(data.test.labels[0:5]))
    # # label for first 5 elements:
    # #  [7 2 1 0 4]

    # with oneshot labels
    data = input_data.read_data_sets("data/MNIST/", one_hot=True)
    print("label for first 5 elements:\n {}".format(data.test.labels[0:5, :]))
    # label for first 5 elements:
    #  [[0. 0. 0. 0. 0. 0. 0. 1. 0. 0.]
    #  [0. 0. 1. 0. 0. 0. 0. 0. 0. 0.]
    #  [0. 1. 0. 0. 0. 0. 0. 0. 0. 0.]
    #  [1. 0. 0. 0. 0. 0. 0. 0. 0. 0.]
    #  [0. 0. 0. 0. 1. 0. 0. 0. 0. 0.]]


    # print("Size of: ")
    # print("Traing set: {}".format(len(data.train.labels)))  # 55000
    # print("Test set: {}".format(len(data.test.labels)))     # 10000
    # print("Validation set: {}".format(len(data.validation.labels))) # 5000


    # MNIST image shape is 28x28 pixel
    img_size = 28
    img_shape = (img_size, img_size) # shape of image
    img_flat_size = img_size * img_size # size after flatting the matrix
    num_labels = 10 # i.e. 0-9

    # convert oneshot to number for displaying in plots 
    data.test.numbers = np.array([label.argmax() for label in data.test.labels])

    # plot first 9 images
    images = data.test.images[0:9]
    true_images = data.test.numbers[0:9]
    # plot images
    plot_images = plot_mnist_image(images=images, 
        label_true=true_images, 
        label_predict=None,  # as no prediction made yet 
        img_shape=img_shape
    )



.. code-block:: python
    :linenos:
    :caption: Helper function to plot the mnist data

    # helper_functions.py

    import matplotlib.pyplot as plt

    def plot_mnist_image(images, label_true, label_predict, img_shape=(28, 28)):
        fig, axes = plt.subplots(3, 3) # 3x3 plots

        for i, ax in enumerate(axes.flat):
            # plot image
            ax.imshow(images[i].reshape(img_shape), cmap='binary')

            # show true and predicted labels
            if label_predict is None:
                xlabel = "True: {}".format(label_true[i])
            else:
                xlabel = "True: {}, Predict: {}".format(label_true[i], label_predict[i])

            ax.set_xlabel(xlabel)

        plt.show()


.. _`fig_mnist1`:

.. figure:: img/mnist1.jpg
    :width: 75%

    First nine numbers in test data



Create tensorflow graph
=======================


* create placeholder for input

.. code-block:: python
    :linenos:
    :caption: create placeholder for input

    # mnist_ex.py

    [...]

    # create placeholder for input value
    # type float32, rows size = any number, column size = img_flat_size
    x = tf.placeholder(tf.float32, [None, img_flat_size])  # store data of image
    y = tf.placeholder(tf.float32, [None, num_labels])     # store one_hot labels 
    # store numbered lables i.e. corresponding values to one_hot labels
    y_number = tf.placeholder(tf.int64, [None])          


* create variable of tensorflow

.. code-block:: python
    :linenos:
    :caption: create variables of tensorflow

    # mnist_ex.py

    [...]


    # create variables of tensorflow
    # weights
    weights = tf.Variable(tf.zeros([img_flat_size, num_labels]))
    # bias
    bias = tf.Variable(tf.zeros([num_labels]))


Create model
============

A simple neural network model can be defined as "**output = input * weights + bias**". This model can be defined using tensorflow as shown in :numref:`py_model`


.. code-block:: python
    :linenos:
    :caption: create model
    :name: py_model

    # mnist_ex.py

    [...]

    # create model
    logits = tf.matmul(x, weights) + bias

    # convert above results into probability density using 
    # softmax fuction i.e. sum of the vector will be 1
    y_pred = tf.nn.softmax(logits)
    # store the index-number which has highest value
    y_pred_number = tf.argmax(y_pred, dimension=1) 


.. note:: 

    The :numref:`py_model` only creates the model and does not execute the statement.   



Execute the model
=================

Let's execute the model now without any optimization. Since, all the weights are initialized as zero, and no optimization is performed, therefore all the numbers will be predicted as zero. 


.. code-block:: python
    :linenos:
    :caption: Execute the model
    :name: py_execute_model

    # mnist_ex.py

    [...]


    # Create session and check the result: 
    # since all the weights are zero, # therefore all the values 
    # will be detected as zero

    # create session
    session = tf.Session()
    # initialize variables
    session.run(tf.global_variables_initializer())

    # assign value to placeholder
    feed_dict = {
        x: data.train.images,
        y: data.train.labels
    }

    # calculate y_pred_number and save in 'result'
    result = session.run(y_pred_number, feed_dict=feed_dict)
    # plot first 9 result
    plot_images = plot_mnist_image(images=images, 
        label_true=true_images, 
        label_predict=result[0:9],  
        img_shape=img_shape
    )

    # Sum is 0 as all the prediction are zero
    print(sum(result)) # 0


* The completed code is shown below, 

.. code-block:: python
    :linenos:
    :caption: Complete 'mnist_ex.py' code till now

    # mnist_ex.py

    # MNIST dataset is a collection of hand-written digits-images. 
    # Training set = 55000
    # Test set = 10000
    # Validation set = 5000

    import numpy as np
    import tensorflow as tf

    from tensorflow.examples.tutorials.mnist import input_data

    from helper_functions import plot_mnist_image


    # data will be donwloaded to './data/MNIST' 
    # one_hot convert each number to vector with single 1, 
    # e.g. 0 = [1, 0, 0, 0, 0, 0, 0, 0]  (assuming total 8 labels)
    # and 4 = [0, 0, 0, 0, 1, 0, 0, 0]

    # # without oneshot labels
    # data = input_data.read_data_sets("data/MNIST/")
    # print("label for first 5 elements:\n {}".format(data.test.labels[0:5]))
    # # label for first 5 elements:
    # #  [7 2 1 0 4]

    # with oneshot labels
    data = input_data.read_data_sets("data/MNIST/", one_hot=True)
    print("label for first 5 elements:\n {}".format(data.test.labels[0:5, :]))
    # label for first 5 elements:
    #  [[0. 0. 0. 0. 0. 0. 0. 1. 0. 0.]
    #  [0. 0. 1. 0. 0. 0. 0. 0. 0. 0.]
    #  [0. 1. 0. 0. 0. 0. 0. 0. 0. 0.]
    #  [1. 0. 0. 0. 0. 0. 0. 0. 0. 0.]
    #  [0. 0. 0. 0. 1. 0. 0. 0. 0. 0.]]


    # print("Size of: ")
    # print("Traing set: {}".format(len(data.train.labels)))  # 55000
    # print("Test set: {}".format(len(data.test.labels)))     # 10000
    # print("Validation set: {}".format(len(data.validation.labels))) # 5000


    # MNIST image shape is 28x28 pixel
    img_size = 28
    img_shape = (img_size, img_size) # shape of image
    img_flat_size = img_size * img_size # size after flatting the matrix
    num_labels = 10 # i.e. 0-9

    # convert oneshot to number for displaying in plots 
    data.test.numbers = np.array([label.argmax() for label in data.test.labels])

    # plot first 9 images
    images = data.test.images[0:9]
    true_images = data.test.numbers[0:9]
    # plot images
    plot_images = plot_mnist_image(images=images, 
        label_true=true_images, 
        label_predict=None,  # as no prediction made yet 
        img_shape=img_shape
    )


    # create placeholder for input value
    # type float32, rows size = any number, column size = img_flat_size
    x = tf.placeholder(tf.float32, [None, img_flat_size])  # store data of image
    y = tf.placeholder(tf.float32, [None, num_labels])     # store one_hot labels 
    # store numbered lables i.e. corresponding values to one_hot labels
    y_number = tf.placeholder(tf.int64, [None])          


    # create variables of tensorflow
    # weights
    weights = tf.Variable(tf.zeros([img_flat_size, num_labels]))
    # bias
    bias = tf.Variable(tf.zeros([num_labels]))


    # create model
    logits = tf.matmul(x, weights) + bias

    # convert above results into probability density using 
    # softmax fuction i.e. sum of the vector will be 1
    y_pred = tf.nn.softmax(logits)
    # store the index-number which has highest value
    y_pred_number = tf.argmax(y_pred, dimension=1) 


    # Create session and check the result: 
    # since all the weights are zero, # therefore all the values 
    # will be detected as zero

    # create session
    session = tf.Session()
    # initialize variables
    session.run(tf.global_variables_initializer())

    # assign value to placeholder
    feed_dict = {
        x: data.train.images,
        y: data.train.labels
    }

    # calculate y_pred_number and save in 'result'
    result = session.run(y_pred_number, feed_dict=feed_dict)
    # plot first 9 result
    plot_images = plot_mnist_image(images=images, 
        label_true=true_images, 
        label_predict=result[0:9],  
        img_shape=img_shape
    )

    # Sum is 0 as all the prediction are zero
    print(sum(result)) # 0



.. _`fig_mnist2`:

.. figure:: img/mnist2.jpg
    :width: 75%

    All the numbers are predicted as 0




Use Gradient optimizer for prediction
=====================================

We need to define the 'cost function' to use the Gradient optimizer. The optimizer will try to reduce the cost function. In the below code, 'cross entropy' is used as the cost fuction. 


.. code-block:: python
    :linenos:
    :caption: Use Gradient optimizer for prediction

    # mnist.py

    # MNIST dataset is a collection of hand-written digits-images. 
    # Training set = 55000
    # Test set = 10000
    # Validation set = 5000

    import numpy as np
    import tensorflow as tf

    from tensorflow.examples.tutorials.mnist import input_data

    from helper_functions import plot_mnist_image


    # data will be donwloaded to './data/MNIST' 
    # one_hot convert each number to vector with single 1, 
    # e.g. 0 = [1, 0, 0, 0, 0, 0, 0, 0]  (assuming total 8 labels)
    # and 4 = [0, 0, 0, 0, 1, 0, 0, 0]

    # # without oneshot labels
    # data = input_data.read_data_sets("data/MNIST/")
    # print("label for first 5 elements:\n {}".format(data.test.labels[0:5]))
    # # label for first 5 elements:
    # #  [7 2 1 0 4]

    # with oneshot labels
    data = input_data.read_data_sets("data/MNIST/", one_hot=True)
    # print("label for first 5 elements:\n {}".format(data.test.labels[0:5, :]))
    # # label for first 5 elements:
    # #  [[0. 0. 0. 0. 0. 0. 0. 1. 0. 0.]
    # #  [0. 0. 1. 0. 0. 0. 0. 0. 0. 0.]
    # #  [0. 1. 0. 0. 0. 0. 0. 0. 0. 0.]
    # #  [1. 0. 0. 0. 0. 0. 0. 0. 0. 0.]
    # #  [0. 0. 0. 0. 1. 0. 0. 0. 0. 0.]]


    # print("Size of: ")
    # print("Traing set: {}".format(len(data.train.labels)))  # 55000
    # print("Test set: {}".format(len(data.test.labels)))     # 10000
    # print("Validation set: {}".format(len(data.validation.labels))) # 5000


    # MNIST image shape is 28x28 pixel
    img_size = 28
    img_shape = (img_size, img_size) # shape of image
    img_flat_size = img_size * img_size # size after flatting the matrix
    num_labels = 10 # i.e. 0-9

    # convert oneshot to number for displaying in plots 
    data.test.numbers = np.array([label.argmax() for label in data.test.labels])

    # plot first 9 images
    images = data.test.images[0:9]
    true_images = data.test.numbers[0:9]
    # # plot images
    # plot_images = plot_mnist_image(images=images, 
    #     label_true=true_images, 
    #     label_predict=None,  # as no prediction made yet 
    #     img_shape=img_shape
    # )


    # create placeholder for input value
    # type float32, rows size = any number, column size = img_flat_size
    x = tf.placeholder(tf.float32, [None, img_flat_size])  # store data of image
    y = tf.placeholder(tf.float32, [None, num_labels])     # store one_hot labels 
    # store numbered lables i.e. corresponding values to one_hot labels
    y_number = tf.placeholder(tf.int64, [None])          


    # create variables of tensorflow
    # weights
    weights = tf.Variable(tf.zeros([img_flat_size, num_labels]))
    # bias
    bias = tf.Variable(tf.zeros([num_labels]))


    # create model
    logits = tf.matmul(x, weights) + bias

    # convert above results into probability density using 
    # softmax fuction i.e. sum of the vector will be 1
    y_pred = tf.nn.softmax(logits)
    # store the index-number which has highest value
    y_pred_number = tf.argmax(y_pred, dimension=1) 


    # Define cost-function for optimization : i.e. corss-entropy here
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=logits))
    # use optimizer on cost function "cross-entropy"
    optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    # Evaluate accuracy
    # correct prediction i.e. predicted = actual
    correct_prediction = tf.equal(y_pred_number, y_number)
    # accuracy = correct_prediction/total_images
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


    # Create session and check the result: 
    # since all the weights are zero, # therefore all the values 
    # will be detected as zero

    # create session
    session = tf.Session()
    # initialize variables
    session.run(tf.global_variables_initializer())

    # assign value to placeholder
    feed_dict = {
        x: data.train.images,
        y: data.train.labels
    }


    session.run(optimizer, feed_dict=feed_dict)
    # # or run in batch (more accurate than above)
    # repeat = 4000
    # for i in range(repeat): # run 'repeat' times by selecting 100 data randomly
    #   # load data in a batch of 100 
    #   batch = data.train.next_batch(100)
    #   # run optimizer for 100 data
    #   session.run(optimizer, feed_dict={x: batch[0], y: batch[1]})

    feed_dict_accuracy = {
        x: data.test.images, 
        y: data.test.labels, 
        y_number: data.test.numbers
    }

    prediction, accuracy = (session.run([y_pred_number, accuracy], feed_dict=feed_dict_accuracy))
    print("Accuracy = ", accuracy) # Accuracy =  0.6705, for batch method->Accuracy =  0.9238

    # plot first 9 result
    plot_images = plot_mnist_image(images=images, 
        label_true=true_images, 
        label_predict=prediction[0:9],  
        img_shape=img_shape
    )


.. _`fig_mnist3`:

.. figure:: img/mnist3.jpg
    :width: 75%

    Accuracy improved by using Gradient optimizer for prediction
