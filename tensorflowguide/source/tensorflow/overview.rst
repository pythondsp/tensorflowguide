.. _`ch_overview`:

Tensorflow Overview
*******************

Introduction
============

* Operates over Tensor (i.e. n-dim array)
  

Basic component of tensorflow
=============================


* **Graph** : The tensorflow computational steps are represented as a dataflow graph. 

    - It is a collection of operations that may execute together as a group. 
    - Since Tensorflow has the complete computational graph in the beginning of the calcuation, therefore it is more efficient than the numpy which knows only one operation at a time.
    - Due to complete graph, Tensorflow can calculate the gradient which are required for optimized solutions. 
    - Tensorflow can use both CPU, GPU and TPU (tensor processing unit). 

* **Operations**: It is a graph node that performs computation on tensors. 
* **Tensor**: It handles the one of the outputs of an Operations. 

    - provide a means of computing a value in a Tensorflow session. 
    
* **Placeholder variable**: It is used as input to the graph, which changes its value during graph-execution. In the other words, all the input values are passed to graph via placeholder.  
* **Variables**: a modifiable tensor that lives in Tensorflow's graph of interacting operations. Values of these variables are changed by tensorflow, so that model can perform better on the training data. 
* **Sessions**:  encapsulate the environment in which operations objects are executed, and tensor objects are calculated. 
  

.. code-block:: python
    :linenos:
    :name: Basic component of Tensorflow

    # ex1.py

    import numpy as np
    import tensorflow as tf


    # constant matrix from numpy
    m1 = np.array([[1., 2.], [3., 4.], [5., 6.], [7., 8.]], dtype=np.float32)
    # A placeholder to read the numpy input data.
    m1_input = tf.placeholder(tf.float32, shape=[4, 2])

    # variable using 'tensorflow'
    m2 = tf.Variable(tf.random_uniform([2, 3], -1.0, 1.0))

    # multiply matrix m1_input and m2
    # note that we did not send m1 directly but the placeholder m1_input
    # also, define below line does not multiply immediatley 
    # it is just a grpah which contains the list of operations. 
    m3 = tf.matmul(m1_input, m2)


    # Add variable initializer. It is rquired to intialize all the variables. 
    init = tf.global_variables_initializer()  


    # all the above commands will be run in the session. 
    with tf.Session() as session:
        # We must initialize all variables before we use them.
        init.run()
        # print("Initialized")

        print("m2: {}".format(m2))  # m2: <tf.Variable 'Variable:0' shape=(2, 3) dtype=float32_ref>
        print("eval m2: {}".format(m2.eval()))  # display the value in m2 (tensorflow-variable)

        # map input value to placeholder, so that it can be used by tensorflow
        map_inputs_dict = {m1_input: m1}

        # calculate m3 based on values in map_inputs_dict
        result = session.run([m3], feed_dict=map_inputs_dict)
        print("\nresult: {}\n".format(result))


* Let's add the result m3 to itself, 
  
.. code-block:: python
    :linenos:
    :emphasize-lines: 21-22, 41-44
    :name: m3 + m3

    # ex1.py

    import numpy as np
    import tensorflow as tf


    # constant matrix from numpy
    m1 = np.array([[1., 2.], [3., 4.], [5., 6.], [7., 8.]], dtype=np.float32)
    # A placeholder to read the numpy input data.
    m1_input = tf.placeholder(tf.float32, shape=[4, 2])

    # variable using 'tensorflow'
    m2 = tf.Variable(tf.random_uniform([2, 3], -1.0, 1.0))

    # multiply matrix m1_input and m2
    # note that we did not send m1 directly but the placeholder m1_input
    # also, define below line does not multiply immediatley 
    # it is just a grpah which contains the list of operations. 
    m3 = tf.matmul(m1_input, m2)

    # add result m3 to itself
    m4 = tf.add(m3, m3)


    # Add variable initializer. It is rquired to intialize all the variables. 
    init = tf.global_variables_initializer()  


    # all the above commands will be run in the session. 
    with tf.Session() as session:
        # We must initialize all variables before we use them.
        init.run()
        # print("Initialized")

        print("m2: {}".format(m2))  # m2: <tf.Variable 'Variable:0' shape=(2, 3) dtype=float32_ref>
        print("eval m2: {}".format(m2.eval()))  # display the value in m2 (tensorflow-variable)

        # map input value to placeholder, so that it can be used by tensorflow
        map_inputs_dict = {m1_input: m1}

        # calculate m3 based on values in map_inputs_dict
        mul1, sum1 = session.run([m3, m4], feed_dict=map_inputs_dict)
        print("m3: {}\n".format(mul1))
        print("m4: {}\n".format(sum1))
