Simple Models
*************

Introduction
============

In this chapter, we will implement some of the models using tensorflow. 



Linear Regression model
=======================

The equation for linear regression model is given by :math:`y = wx`

Generate data
-------------

.. code-block:: python
    :linenos: 

    # linear_reg.py

    import numpy as np
    import matplotlib.pyplot as plt
    import tensorflow as tf

    def generate_data(): 
        x_train = np.linspace(-1, 1, 100)

        # *x_train.shape results the value of tuple i.e. 3 for (3, )
        y_train = 0.4 * np.random.randn(*x_train.shape)  + 2 * x_train

        return x_train, y_train

    if __name__=='__main__':
        x_train, y_train = generate_data()
        plt.scatter(x_train, y_train)
        plt.show()


* Below is the output, 

.. code-block:: shell

    $ python linear_reg.py


.. _`fig_Screenshot_1`:

.. figure:: img/Screenshot_1.jpg
    :width: 75%

    Scatter data : y = 2 * x + 0.4 * noise 



Linear regression model
-----------------------


.. code-block:: python
    :linenos: 

    import numpy as np
    import matplotlib.pyplot as plt
    import tensorflow as tf

    def generate_data(): 
        x_train = np.linspace(-1, 1, 100)

        # *x_train.shape results the value of tuple i.e. 3 for (3, )
        y_train = 0.4 * np.random.randn(*x_train.shape)  + 2 * x_train

        return x_train, y_train

    def linear_reg_model(X, w):

        return tf.multiply(X, w)

    def run_session(x_train, y_train, learning_rate, epochs): 
        X = tf.placeholder(tf.float32) # training input
        Y = tf.placeholder(tf.float32) # expected output

        # weights
        w = tf.Variable(0.0, name="weights") 

        # linear regression model 
        y_model = linear_reg_model(X, w)

        # cost
        cost = tf.square(Y - y_model)

        # optimizer
        train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
        
        # start session 
        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)

        for epoch in range(epochs): 
            for (x, y) in zip(x_train, y_train): 
                sess.run(train_op, feed_dict={X: x, Y: y}) 
            w_val = sess.run(w) 
        
        sess.close() 

        return w_val 

    if __name__=='__main__':
        x_train, y_train = generate_data()
        plt.scatter(x_train, y_train)

        w_val = run_session(x_train, y_train, learning_rate=0.01, epochs=100)
        y_estimated = x_train * w_val
        plt.plot(x_train, y_estimated, '-r')

        plt.show()


* Below is the output, 

.. code-block:: shell

    $ python linear_reg.py


.. _`fig_Screenshot_2`:

.. figure:: img/Screenshot_2.jpg
    :width: 75%

    Estimated values by the linear regression model 



Polynomial model
================

The equation for polynomial model is given by :math:`y = w_n x^n + ... + w_1 x + w_0`


Apply polynomial model on linear data
-------------------------------------

In this section, we used the polynomial model on the linear data which is generated in last section, 

.. code-block:: python
    :linenos: 

    # polynomial_ex.py

    import numpy as np
    import matplotlib.pyplot as plt
    import tensorflow as tf

    def generate_data(): 
        x_train = np.linspace(-1, 1, 100)

        # *x_train.shape results the value of tuple i.e. 3 for (3, )
        y_train = 0.4 * np.random.randn(*x_train.shape)  + 2 * x_train

        return x_train, y_train

    def polynomial_model(X, w, num_coeff):
        terms = []
        for i in range(num_coeff): 
            terms.append(tf.multiply(w[i], tf.pow(X, i)))
        return tf.add_n(terms)

    def run_session(x_train, y_train, num_coeff, learning_rate, epochs): 
        X = tf.placeholder(tf.float32) # training input
        Y = tf.placeholder(tf.float32) # expected output

        # weights
        w = tf.Variable([0.] * num_coeff, name="weights")

        # linear regression model 
        y_model = polynomial_model(X, w, num_coeff=num_coeff)

        # cost
        cost = tf.square(Y - y_model)

        # optimizer
        train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
        
        # start session 
        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)

        for epoch in range(epochs): 
            for (x, y) in zip(x_train, y_train): 
                sess.run(train_op, feed_dict={X: x, Y: y}) 
            w_val = sess.run(w) 
        
        sess.close() 

        return w_val 

    if __name__=='__main__':
        x_train, y_train = generate_data()
        plt.scatter(x_train, y_train)

        num_coeff = 6
        w_val = run_session(x_train, y_train, num_coeff=num_coeff, learning_rate=0.01, epochs=100)
        
        y_estimated = 0
        for i in range(num_coeff):
            y_estimated += w_val[i] * np.power(x_train, i)

        plt.plot(x_train, y_estimated, '-r')

        plt.show()


.. _`fig_Screenshot_3`:

.. figure:: img/Screenshot_3.jpg
    :width: 75%

    Polynomial model on linear data 


Apply polynomial model on non linear data
-----------------------------------------

Now, generate the nonlinear data in function "generate_data" and see the performance of the code. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 11-13

    # polynomial_ex.py

    import numpy as np
    import matplotlib.pyplot as plt
    import tensorflow as tf

    def generate_data(): 
        x_train = np.linspace(-1, 1, 100)

        # *x_train.shape results the value of tuple i.e. 3 for (3, )
        y_train = 0
        for i in range(10):
            y_train += 0.4 * np.random.randn(*x_train.shape)  + 2 * np.power(x_train, i)

        return x_train, y_train

    def polynomial_model(X, w, num_coeff):
        terms = []
        for i in range(num_coeff): 
            terms.append(tf.multiply(w[i], tf.pow(X, i)))
        return tf.add_n(terms)

    def run_session(x_train, y_train, num_coeff, learning_rate, epochs): 
        X = tf.placeholder(tf.float32) # training input
        Y = tf.placeholder(tf.float32) # expected output

        # weights
        w = tf.Variable([0.] * num_coeff, name="weights")

        # linear regression model 
        y_model = polynomial_model(X, w, num_coeff=num_coeff)

        # cost
        cost = tf.square(Y - y_model)

        # optimizer
        train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
        
        # start session 
        sess = tf.Session()
        init = tf.global_variables_initializer()
        sess.run(init)

        for epoch in range(epochs): 
            for (x, y) in zip(x_train, y_train): 
                sess.run(train_op, feed_dict={X: x, Y: y}) 
            w_val = sess.run(w) 
        
        sess.close() 

        return w_val 

    if __name__=='__main__':
        x_train, y_train = generate_data()
        plt.scatter(x_train, y_train)

        num_coeff = 6
        w_val = run_session(x_train, y_train, num_coeff=num_coeff, learning_rate=0.01, epochs=100)
        
        y_estimated = 0
        for i in range(num_coeff):
            y_estimated += w_val[i] * np.power(x_train, i)

        plt.plot(x_train, y_estimated, '-r')

        plt.show()


.. _`fig_Screenshot_4`:

.. figure:: img/Screenshot_4.jpg
    :width: 75%

    Polynomial model on nonlinear data