Tensorflow examples
*******************


Introduction
============


In :numref:`Chapter %s <ch_overview>`, we saw an overview to Tensorflow. In this chapter, we will see various operations which are available in the tensorflow. From the next chapter, we will use these operations to implement the machine learning algorithm. 



Convert to tensorflow
======================


.. code-block:: python
    :linenos:

    >>> import tensorflow as tf

    # list to tensorflow conversion 
    >>> m1 = [ [1, 2], [3, 4] ]
    >>> type(m1)
    <class 'list'>
    >>> t1 = tf.convert_to_tensor(m1, dtype=tf.float32)
    >>> type(t1)
    <class 'tensorflow.python.framework.ops.Tensor'>


    # numpy to tensorflow conversion
    >>> import numpy as np
    >>> m2 = np.array([[1, 2], [3, 4]], dtype=np.float32)
    >>> type(m2)
    <class 'numpy.ndarray'>
    >>> t2 = tf.convert_to_tensor(m2, dtype=tf.float32)
    >>> type(t2)
    <class 'tensorflow.python.framework.ops.Tensor'>



Operations
==========

In previous section, we saw the methods which is used convert the list or numpy array into tensors. Now, we will perform some operatoins on the tensorflow. The complete list of oprations can be found at `tf.math <https://www.tensorflow.org/api_docs/python/tf/math>`_. 


.. code-block:: python
    :linenos: 
 
     
    >>> import tensorflow as tf

    # calculate -ve
    >>> a = tf.constant([ [1, 2, 3, 4] ])
    >>> b = tf.negative(a)
    >>> print(b)
    Tensor("Neg:0", shape=(1, 4), dtype=int32)
    >>> 
    >>> with tf.Session() as sess:
    ...     result = sess.run(b)
    ...     print(result)
    ... 
    [[-1 -2 -3 -4]]

    # add two tensors 
    >>> sum = tf.add(a, b); 
    >>> with tf.Session() as sess: 
    ...     sum = sess.run(sum)
    ...     print(sum)
    ... 
    [[0 0 0 0]]



Save the tensorflow variables
=============================


.. code-block:: python
    :linenos: 

    # save_tf.py

    import tensorflow as tf

    a = tf.constant([ [1, 2, 3, 4] ], name="a")
    b = tf.Variable(tf.random_normal(shape=[2]), name="b")
    c = tf.Variable(tf.random_normal(shape=[2,2]), name="c")

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    with tf.Session() as sess:
        init.run()
        d = tf.multiply(a, 2)
        e = tf.multiply(c, 3)
        saver_path = saver.save(sess, "./meher.ckpt")
     
        print("\n\n")
        print("a = ", a.eval())
        print("b = ", b.eval())
        print("c = ", c.eval())
        print("d = ", d.eval())
        print("\n\nSaver Path = ", saver_path)

    print("\n\nSaved items in the checkpoint are below:\n")
    # see all values which are stored in the checkpoint
    from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
    latest_ckp = tf.train.latest_checkpoint('./')
    print_tensors_in_checkpoint_file(latest_ckp, all_tensors=True, tensor_name='')




* Below is the output, 

.. code-block:: shell

    a =  [[1 2 3 4]]
    b =  [0.5460819 0.6374034]
    c =  [[ 0.09093301 -0.3235254 ]
     [-0.18190224  0.01609637]]
    d =  [[2 4 6 8]]


    Saver Path =  ./meher.ckpt


    Saved items in the checkpoint are below:

    tensor_name:  b
    [0.5460819 0.6374034]

    tensor_name:  c
    [[ 0.09093301 -0.3235254 ]
     [-0.18190224  0.01609637]]




Load the tensorflow variables
=============================

.. code-block:: python

    # load_tf.py 

    import tensorflow as tf

    with tf.Session() as sess:    
        saver = tf.train.import_meta_graph('meher.ckpt.meta')
        saver.restore(sess,tf.train.latest_checkpoint('./'))
        
        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("b:0")
        y = graph.get_tensor_by_name("c:0")

        print("x = ", x.eval())
        print("y = ", y.eval())



* Below is the output, 

.. code-block:: shell

    $ python load_tf.py

    x =  [0.5460819 0.6374034]
    y =  [[ 0.09093301 -0.3235254 ]
     [-0.18190224  0.01609637]]


Implement v = u + at
====================

.. code-block:: python
    :linenos: 

    # eq_test.py

    import tensorflow as tf
    import numpy as np

    sample_data = np.arange(1, 10)

    time = tf.placeholder(tf.float32, name="time")
    initial_speed = tf.constant(10., name="initial_speed")
    acc = tf.Variable(2., name="acc")


    final_speed = initial_speed + acc * time
    init = tf.global_variables_initializer()

    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run(init)

        print("\n\n")
        print("Initial acceleration is 2")
        print("Final speed = 10 + 2 * t")
        for i in range(len(sample_data)): 
            inst_speed = sess.run(final_speed, feed_dict = {time : sample_data[i]})
            print("Time: {}, Final speed: {}".format(sample_data[i], inst_speed))


        print("\n\n")
        print("Changing acceleration to 4")
        print("Final speed = 10 + 4 * t")
        sess.run(tf.assign(acc, 4.0)) 
        for i in range(len(sample_data)): 
            inst_speed = sess.run(final_speed, feed_dict = {time : sample_data[i]})
            print("Time: {}, Final speed: {}".format(sample_data[i], inst_speed))

        saver_path = saver.save(sess, "./eq_test.ckpt")
        print("\n\nSaver Path = ", saver_path)

    print("\n\nSaved items in the checkpoint are below:\n")
    # see all values which are stored in the checkpoint
    from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
    latest_ckp = tf.train.latest_checkpoint('./')
    print_tensors_in_checkpoint_file(latest_ckp, all_tensors=True, tensor_name='')






* Below is the output, 
  
.. code-block:: shell

    $ python eq_test.py

    Initial acceleration is 2
    Final speed = 10 + 2 * t
    Time: 1, Final speed: 12.0
    Time: 2, Final speed: 14.0
    Time: 3, Final speed: 16.0
    Time: 4, Final speed: 18.0
    Time: 5, Final speed: 20.0
    Time: 6, Final speed: 22.0
    Time: 7, Final speed: 24.0
    Time: 8, Final speed: 26.0
    Time: 9, Final speed: 28.0



    Changing acceleration to 4
    Final speed = 10 + 4 * t
    Time: 1, Final speed: 14.0
    Time: 2, Final speed: 18.0
    Time: 3, Final speed: 22.0
    Time: 4, Final speed: 26.0
    Time: 5, Final speed: 30.0
    Time: 6, Final speed: 34.0
    Time: 7, Final speed: 38.0
    Time: 8, Final speed: 42.0
    Time: 9, Final speed: 46.0


    Saver Path =  ./eq_test.ckpt


    Saved items in the checkpoint are below:

    tensor_name:  acc
    4.0


.. note::

    The updated value of the acc (i.e. 4) is stored in the checkpoint (initial was 2). 


Plot tesnorflow graph
=====================


.. code-block:: python
    :linenos:
    :emphasize-lines: 16, 22-26, 32, 34, 42, 44

    # eq_test.py

    import tensorflow as tf
    import numpy as np

    sample_data = np.arange(1, 10)

    time = tf.placeholder(tf.float32, name="time")
    initial_speed = tf.constant(10., name="initial_speed")
    acc = tf.Variable(2., name="acc")


    final_speed = initial_speed + acc * time
    init = tf.global_variables_initializer()

    writer = tf.summary.FileWriter("./log/")
    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run(init)

        # add 3 summary, therefore 3 graph will be generated
        tf.summary.scalar('acc', acc)
        tf.summary.scalar('time', time)
        tf.summary.scalar('final_speed', final_speed)
        merged = tf.summary.merge_all()  # merge above graph

        print("\n\n")
        print("Initial acceleration is 2")
        print("Final speed = 10 + 2 * t")
        for i in range(int(len(sample_data)/2)): 
            summary, inst_speed = sess.run([merged, final_speed], feed_dict = {time : sample_data[i]})
            print("Time: {}, Final speed: {}".format(sample_data[i], inst_speed))
            writer.add_summary(summary, i)


        print("\n\n")
        print("Changing acceleration to 4")
        print("Final speed = 10 + 4 * t")
        sess.run(tf.assign(acc, 4.0)) 
        for i in range(int(len(sample_data)/2), len(sample_data)): 
            summary, inst_speed = sess.run([merged, final_speed], feed_dict = {time : sample_data[i]})
            print("Time: {}, Final speed: {}".format(sample_data[i], inst_speed))
            writer.add_summary(summary, i)

        saver_path = saver.save(sess, "./eq_test.ckpt")
        print("\n\nSaver Path = ", saver_path)


    print("\n\nSaved items in the checkpoint are below:\n")
    # see all values which are stored in the checkpoint
    from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
    latest_ckp = tf.train.latest_checkpoint('./')
    print_tensors_in_checkpoint_file(latest_ckp, all_tensors=True, tensor_name='')


See the graph as below, 

.. code-block:: shell

    # remove the log folder
    $ rm -r log/

    # run the eq_test.py file
    $ python eq_test.py

    # see the graph
    $ tensorboard --logdir=./log/
    TensorBoard 1.9.0 at http://mekrip:6006 (Press CTRL+C to quit)



* Below are the graph for acceleration, speed and time

.. _`fig_basic1`:

.. figure:: img/basic1.jpg
    :width: 75%

    Acceleration


.. _`fig_basic2`:

.. figure:: img/basic2.jpg
    :width: 75%

    Speed


.. _`fig_basic3`:

.. figure:: img/basic3.jpg
    :width: 75%

    Time



Questions
=========

1. Generate Gaussian distribution 


.. code-block:: shell
    :linenos:

    from math import pi
    mean = 0.0
    sigma = 1.0
    (tf.exp(tf.negative(tf.pow(x – mean, 2.0) /
    (2.0 * tf.pow(sigma, 2.0) ))) *
    (1.0 / (sigma * tf.sqrt(2.0 * pi) )))