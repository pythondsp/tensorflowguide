# load_tf.py 

import tensorflow as tf

with tf.Session() as sess:    
    saver = tf.train.import_meta_graph('meher.ckpt.meta')
    saver.restore(sess,tf.train.latest_checkpoint('./'))
    
    graph = tf.get_default_graph()
    x = graph.get_tensor_by_name("b:0")
    y = graph.get_tensor_by_name("c:0")

    print("x = ", x.eval())
    print("y = ", y.eval())

