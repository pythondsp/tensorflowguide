# linear_reg.py

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

def generate_data(): 
    x_train = np.linspace(-1, 1, 100)

    # *x_train.shape results the value of tuple i.e. 3 for (3, )
    y_train = 0.4 * np.random.randn(*x_train.shape)  + 2 * x_train

    return x_train, y_train

def linear_reg_model(X, w):

    return tf.multiply(X, w)

def run_session(x_train, y_train, learning_rate, epochs): 
    X = tf.placeholder(tf.float32) # training input
    Y = tf.placeholder(tf.float32) # expected output

    # weights
    w = tf.Variable(0.0, name="weights") 

    # linear regression model 
    y_model = linear_reg_model(X, w)

    # cost
    cost = tf.square(Y - y_model)

    # optimizer
    train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
    
    # start session 
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    for epoch in range(epochs): 
        for (x, y) in zip(x_train, y_train): 
            sess.run(train_op, feed_dict={X: x, Y: y}) 
        w_val = sess.run(w) 
    
    sess.close() 

    return w_val 

if __name__=='__main__':
    x_train, y_train = generate_data()
    plt.scatter(x_train, y_train)

    w_val = run_session(x_train, y_train, learning_rate=0.01, epochs=100)
    y_estimated = x_train * w_val
    plt.plot(x_train, y_estimated, '-r')

    plt.show()