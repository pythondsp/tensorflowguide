# fitler_example.py

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf


def plot_filter(conv_filt):
    plt.figure()
    rows, cols = 4, 8
    for i in range(np.shape(conv_filt)[3]):
        img = conv_filt[:, :, 0, i] # i.e 5:5:1:i
        plt.subplot(rows, cols, i + 1) # plot individual filter in subplot
        plt.imshow(img)
        plt.axis('off')
        plt.title('Filter {}'.format(i))
    


if __name__=="__main__":
    # generate 32 one dimensional filter of size 5, 5
    conv_filt = tf.Variable(tf.random_normal([5, 5, 1, 32]))

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        conv_filt_val = sess.run(conv_filt)
        plot_filter(conv_filt_val)
    
    plt.show()