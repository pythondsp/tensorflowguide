# save_tf.py

import tensorflow as tf

a = tf.constant([ [1, 2, 3, 4] ], name="a")
b = tf.Variable(tf.random_normal(shape=[2]), name="b")
c = tf.Variable(tf.random_normal(shape=[2,2]), name="c")

init = tf.global_variables_initializer()
saver = tf.train.Saver()

with tf.Session() as sess:
    init.run()
    d = tf.multiply(a, 2)
    e = tf.multiply(c, 3)
    saver_path = saver.save(sess, "./meher.ckpt")
 
    print("\n\n")
    print("a = ", a.eval())
    print("b = ", b.eval())
    print("c = ", c.eval())
    print("d = ", d.eval())
    print("\n\nSaver Path = ", saver_path)

print("\n\nSaved items in the checkpoint are below:\n")
# see all values which are stored in the checkpoint
from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
latest_ckp = tf.train.latest_checkpoint('./')
print_tensors_in_checkpoint_file(latest_ckp, all_tensors=True, tensor_name='')
