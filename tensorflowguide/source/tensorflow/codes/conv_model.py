# convolve_data.py

import pickle 
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict

def read_data(directory):
    # get classes
    names = unpickle('{}/batches.meta'.format(directory))['label_names']

    # get data and lables 
    data, labels = [], []
    for i in range(1, 6): # there are 5 files: data_batch_1 to data_batch_5
        filename = '{}/data_batch_{}'.format(directory, i)
        batch_data = unpickle(filename)
        # # dict_keys(['batch_label', 'labels', 'data', 'filenames'])
        # print(batch_data.keys())
        if len(data) == 0: 
            data = batch_data['data'] # extract data from the dict
            labels = batch_data['labels'] # extract key from the dict
        else:
            # append data from next dict
            data = np.vstack((data, batch_data['data'])) 
            # append labels from next dict
            labels = np.hstack((labels, batch_data['labels'])) 
    return names, data, labels


def show_conv_results(data, rows=4, cols=4):
    plt.figure()
    rows, cols = rows, cols
    for i in range(np.shape(data)[3]):
        img = data[0, :, :, i]
        plt.subplot(rows, cols, i + 1)
        plt.imshow(img)
        plt.title("Filter {}".format(i))
        plt.axis('off')

def maxpool_layer(conv, k=2):
    return tf.nn.max_pool(conv, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')


def model():
    x_reshaped = tf.reshape(x, shape=[-1, 32, 32, 1])

    conv_out1 = conv_layer(x_reshaped, W1, b1)
    maxpool_out1 = maxpool_layer(conv_out1)
    norm1 = tf.nn.lrn(maxpool_out1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75)

    conv_out2 = conv_layer(norm1, W2, b2)
    norm2 = tf.nn.lrn(conv_out2, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75)
    maxpool_out2 = maxpool_layer(norm2)

    maxpool_reshaped = tf.reshape(maxpool_out2, [-1, W3.get_shape().as_list()[0]])
    local = tf.add(tf.matmul(maxpool_reshaped, W3), b3)
    local_out = tf.nn.relu(local)
    
    out = tf.add(tf.matmul(local_out, W_out), b_out)
    
    return out



def conv_layer(x, W, b):
    conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
    conv_with_b = tf.nn.bias_add(conv, b)
    conv_out = tf.nn.relu(conv_with_b)
    return conv_out

if __name__=="__main__":
    directory = 'C:/meher/Git/cifar-10-batches-py'
    # names = unpickle('{}/batches.meta'.format(directory))
    # print(names['label_names']) # print class names
    names, data, labels = read_data(directory)
    data = data.astype(np.float32)

    item = 5
    # print("Names: ", names)
    # print("data: ", data[item])
    # print("item: {}, labels: {}, name: {}".format(item, 
    #                     labels[item], names[labels[item]]))

    # rows, cols = 3, 3 # show 9 images
    # for item in range(0, rows*cols):
    #     img = np.reshape(data[item], (3, 32, 32)).transpose(1,2,0)
    #     # img = data[item].reshape((3, 32, 32)).transpose(1,2,0)
    #     plt.subplot(rows, cols, item+1)
    #     plt.imshow(img)
    #     plt.title(names[labels[item]])
    

    x = tf.placeholder(tf.float32, [None, 32 * 32 * 3])
    y = tf.placeholder(tf.float32, [None, len(names)])

    W1 = tf.Variable(tf.random_normal([5, 5, 1, 64]))
    b1 = tf.Variable(tf.random_normal([64]))

    W2 = tf.Variable(tf.random_normal([5, 5, 64, 64]))
    b2 = tf.Variable(tf.random_normal([64]))

    W3 = tf.Variable(tf.random_normal([6*6*64, 3072]))
    b3 = tf.Variable(tf.random_normal([3072]))

    W_out = tf.Variable(tf.random_normal([3072, len(names)]))
    b_out = tf.Variable(tf.random_normal([len(names)]))


    model_op = model()

    cost = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(logits=model_op, labels=y))

    train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(cost)
    correct_pred = tf.equal(tf.argmax(model_op, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))


    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        onehot_labels = tf.one_hot(labels, len(names), on_value=1., off_value=0., axis=-1)
        onehot_vals = sess.run(onehot_labels)
        batch_size = len(data)
        print('batch size', batch_size)
        for j in range(0, 1000):
            print('EPOCH', j)
            for i in range(0, len(data), batch_size):
                batch_data = data[i:i+batch_size, :]
                batch_onehot_vals = onehot_vals[i:i+batch_size, :]
                _, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: batch_data, y: batch_onehot_vals})
                if i % 1000 == 0:
                    print(i, accuracy_val)
            print('DONE WITH EPOCH')


    # plt.tight_layout()
    # plt.show()

