# polynomial_ex.py

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

def generate_data(): 
    x_train = np.linspace(-1, 1, 100)

    # *x_train.shape results the value of tuple i.e. 3 for (3, )
    y_train = 0.4 * np.random.randn(*x_train.shape)  + 2 * x_train

    return x_train, y_train

def polynomial_model(X, w, num_coeff):
    terms = []
    for i in range(num_coeff): 
        terms.append(tf.multiply(w[i], tf.pow(X, i)))
    return tf.add_n(terms)

def run_session(x_train, y_train, num_coeff, learning_rate, epochs): 
    X = tf.placeholder(tf.float32) # training input
    Y = tf.placeholder(tf.float32) # expected output

    # weights
    w = tf.Variable([0.] * num_coeff, name="weights")

    # linear regression model 
    y_model = polynomial_model(X, w, num_coeff=num_coeff)

    # cost
    cost = tf.square(Y - y_model)

    # optimizer
    train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
    
    # start session 
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    for epoch in range(epochs): 
        for (x, y) in zip(x_train, y_train): 
            sess.run(train_op, feed_dict={X: x, Y: y}) 
        w_val = sess.run(w) 
    
    sess.close() 

    return w_val 

if __name__=='__main__':
    x_train, y_train = generate_data()
    plt.scatter(x_train, y_train)

    num_coeff = 6
    w_val = run_session(x_train, y_train, num_coeff=num_coeff, learning_rate=0.01, epochs=100)
    
    y_estimated = 0
    for i in range(num_coeff):
        y_estimated += w_val[i] * np.power(x_train, i)

    plt.plot(x_train, y_estimated, '-r')

    plt.show()