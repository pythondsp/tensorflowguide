# cifar_dataset.py

import pickle 
import numpy as np
import matplotlib.pyplot as plt

def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict

def read_data(directory):
    # get classes
    names = unpickle('{}/batches.meta'.format(directory))['label_names']

    # get data and lables 
    data, labels = [], []
    for i in range(1, 6): # there are 5 files: data_batch_1 to data_batch_5
        filename = '{}/data_batch_{}'.format(directory, i)
        batch_data = unpickle(filename)
        # # dict_keys(['batch_label', 'labels', 'data', 'filenames'])
        # print(batch_data.keys())
        if len(data) == 0: 
            data = batch_data['data'] # extract data from the dict
            labels = batch_data['labels'] # extract key from the dict
        else:
            # append data from next dict
            data = np.vstack((data, batch_data['data'])) 
            # append labels from next dict
            labels = np.hstack((labels, batch_data['labels'])) 
    return names, data, labels

if __name__=="__main__":
    directory = 'C:/meher/Git/cifar-10-batches-py'
    # names = unpickle('{}/batches.meta'.format(directory))
    # print(names['label_names']) # print class names
    names, data, labels = read_data(directory)

    item = 5
    print("Names: ", names)
    print("data: ", data[item])
    print("item: {}, labels: {}, name: {}".format(item, 
                        labels[item], names[labels[item]]))

    rows, cols = 3, 3 # show 9 images
    for item in range(0, rows*cols):
        img = np.reshape(data[item], (3, 32, 32)).transpose(1,2,0)
        # img = data[item].reshape((3, 32, 32)).transpose(1,2,0)
        plt.subplot(rows, cols, item+1)
        plt.imshow(img)
        plt.title(names[labels[item]])
        
    plt.tight_layout()
    plt.show()

