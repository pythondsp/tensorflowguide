# convolve_data.py

import pickle 
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict

def read_data(directory):
    # get classes
    names = unpickle('{}/batches.meta'.format(directory))['label_names']

    # get data and lables 
    data, labels = [], []
    for i in range(1, 6): # there are 5 files: data_batch_1 to data_batch_5
        filename = '{}/data_batch_{}'.format(directory, i)
        batch_data = unpickle(filename)
        # # dict_keys(['batch_label', 'labels', 'data', 'filenames'])
        # print(batch_data.keys())
        if len(data) == 0: 
            data = batch_data['data'] # extract data from the dict
            labels = batch_data['labels'] # extract key from the dict
        else:
            # append data from next dict
            data = np.vstack((data, batch_data['data'])) 
            # append labels from next dict
            labels = np.hstack((labels, batch_data['labels'])) 
    return names, data, labels


def show_conv_results(data, rows=4, cols=4):
    plt.figure()
    rows, cols = rows, cols
    for i in range(np.shape(data)[3]):
        img = data[0, :, :, i]
        plt.subplot(rows, cols, i + 1)
        plt.imshow(img)
        plt.title("Filter {}".format(i))
        plt.axis('off')


if __name__=="__main__":
    directory = 'C:/meher/Git/cifar-10-batches-py'
    # names = unpickle('{}/batches.meta'.format(directory))
    # print(names['label_names']) # print class names
    names, data, labels = read_data(directory)

    item = 5
    # print("Names: ", names)
    # print("data: ", data[item])
    # print("item: {}, labels: {}, name: {}".format(item, 
    #                     labels[item], names[labels[item]]))

    # rows, cols = 3, 3 # show 9 images
    # for item in range(0, rows*cols):
    #     img = np.reshape(data[item], (3, 32, 32)).transpose(1,2,0)
    #     # img = data[item].reshape((3, 32, 32)).transpose(1,2,0)
    #     plt.subplot(rows, cols, item+1)
    #     plt.imshow(img)
    #     plt.title(names[labels[item]])
    

    img = data[item, :]
    img = tf.cast(img, tf.float32)
    img = tf.reshape(img, shape=[-1, 32, 32, 1])


    total_conv_filt = 16
    conv_filt_size = 3
    b = tf.Variable(tf.random_normal([total_conv_filt]))
    conv_filt = tf.Variable(tf.random_normal([conv_filt_size, conv_filt_size, 1, total_conv_filt]))
    conv_with_b = tf.nn.bias_add(conv_filt, b)
    conv_out = tf.nn.relu(conv_with_b)

    k = 3
    maxpool = tf.nn.max_pool(conv_out,
        ksize=[1, k, k, 1],
        strides=[1, k, k, 1],
        padding='SAME')

    conv = tf.nn.conv2d(img, maxpool, strides=[1, 1, 1, 1], padding='SAME')

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        conv_val = sess.run(conv)
        show_conv_results(conv_val, rows=4, cols=4)
        print(np.shape(conv_val))

    # plt.tight_layout()
    plt.show()

