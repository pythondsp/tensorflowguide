.. Tensorflow Guide documentation master file, created by
   sphinx-quickstart on Fri Dec  7 08:42:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tensorflow Guide
================

.. toctree::
    :maxdepth: 3
    :numbered:
    :includehidden:
    :caption: Contents:

    tensorflow/overview
    tensorflow/basics
    tensorflow/models
    tensorflow/cnn
    tensorflow/mnist

